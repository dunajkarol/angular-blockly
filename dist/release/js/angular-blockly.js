var AngularBlocky;
(function (AngularBlocky) {
    var AngularBlockyDirective = (function () {
        function AngularBlockyDirective(http, angularBlocklyOptionsProvider) {
            var _this = this;
            this.restrict = "A";
            // public transclude = true;
            // public replace= false;
            this.scope = {
                "angularBlocklyOptions": "=",
            };
            this.controller = AngularBlocky.AngularBlockyController;
            this.controllerAs = "angularBlockyController";
            this.link = function (scope, element, attrs, controller, transclude) {
                var id;
                var guid;
                element.addClass("angular-blockly");
                if (!attrs.hasOwnProperty("id")) {
                    guid = AngularBlocky.AngularBlockyService.CrateGuid();
                    id = "angular-blockly" + guid;
                    element.attr("id", id);
                }
                else {
                    id = attrs["id"];
                }
                _this.httpService.get(_this.angularBlocklyOptions.getPath() + "files/defaultToolbox.xml")
                    .then(function (result) {
                    var $xml = angular.element(result.data);
                    var toolboxId;
                    if (guid !== undefined && guid !== null && guid.length > 0) {
                        toolboxId = "angular-toolbox-" + guid;
                    }
                    else {
                        toolboxId = id + "-toolbox";
                    }
                    scope.angularBlocklyTooltipId = toolboxId;
                    $xml.attr("id", toolboxId);
                    // remove tool from toolbox
                    if (scope.angularBlocklyOptions && scope.angularBlocklyOptions.disableBlock && Object.keys(scope.angularBlocklyOptions.disableBlock).length > 0) {
                        var keys = Object.keys(scope.angularBlocklyOptions.disableBlock);
                        for (var i = 0; i < keys.length; i++) {
                            var categoryName = keys[i];
                            var category = scope.angularBlocklyOptions.disableBlock[categoryName];
                            if (category && category.length > 0) {
                                for (var j = 0; j < category.length; j++) {
                                    var blockName = category[j];
                                    angular.element($xml[0].querySelector("category#" + categoryName + " block[type='" + blockName + "']")).remove();
                                }
                            }
                            else if (category === null) {
                                angular.element($xml[0].querySelector("category#" + categoryName)).remove();
                            }
                        }
                    }
                    if (scope.angularBlocklyOptions && scope.angularBlocklyOptions.customBlocks && Object.keys(scope.angularBlocklyOptions.customBlocks).length > 0) {
                        var keys = Object.keys(scope.angularBlocklyOptions.customBlocks);
                        for (var i = 0; i < keys.length; i++) {
                            var categoryName = keys[i];
                            var category = scope.angularBlocklyOptions.customBlocks[categoryName];
                            if (category && category.blocks.length > 0) {
                                var $category = $xml.find("category#" + categoryName);
                                if ($category.length === 0) {
                                    $category = angular.element("<category id=\"" + categoryName + "\"></category>");
                                }
                                if (category.color || category.color === 0) {
                                    $category.attr("colour", category.color);
                                }
                                for (var j = 0; j < category.blocks.length; j++) {
                                    var blockName = category.blocks[j];
                                    $category.append("<block type=\"" + blockName + "\"></block>");
                                }
                                $xml.append($category);
                            }
                        }
                    }
                    var body = document.getElementsByTagName("body")[0];
                    angular.element(body).append($xml);
                    controller.init(id, toolboxId);
                });
                // ustaw w kontrolerze id 
                // inicjalizuj blockly
            };
            this.httpService = http;
            this.angularBlocklyOptions = angularBlocklyOptionsProvider;
        }
        return AngularBlockyDirective;
    })();
    AngularBlocky.AngularBlockyDirective = AngularBlockyDirective;
    AngularBlocky.AngularBlockyDirectiveFactory = function (http, angularBlocklyOptionsProvider) {
        return new AngularBlockyDirective(http, angularBlocklyOptionsProvider);
    };
    AngularBlocky.AngularBlockyDirectiveFactory.$inject = ["$http", "AngularBlocklyOptions"];
})(AngularBlocky || (AngularBlocky = {}));

var AngularBlocky;
(function (AngularBlocky) {
    var AngularBlockyController = (function () {
        function AngularBlockyController(angularBlocklyService, angularBlocklyOptionsProvider, $scope, $timeout) {
            var _this = this;
            this.isRtl = function () {
                return AngularBlockyController.LANGUAGE_RTL.indexOf(_this.angularBlocklyService.getLang()) !== -1;
            };
            /**
             * Load blocks saved on App Engine Storage or in session/local storage.
             * @param {string} defaultXml Text representation of default blocks.
             */
            this.loadBlocks = function (defaultXml) {
                var loadOnce;
                if (!_this.initialized) {
                    console.warn("Blokly nie zainicjalizowane");
                    return;
                }
                try {
                    loadOnce = window.sessionStorage["loadOnceBlocks"];
                }
                catch (e) {
                    // Firefox sometimes throws a SecurityError when accessing sessionStorage.
                    // Restarting Firefox fixes this, so it looks like a bug.
                    loadOnce = null;
                }
                if (defaultXml) {
                    // Load the editor with default starting blocks.
                    var xml = Blockly.Xml.textToDom(defaultXml);
                    Blockly.Xml.domToWorkspace(_this.workspace, xml);
                }
                else if ("BlocklyStorage" in window && window.location.hash.length > 1) {
                    // An href with #key trigers an AJAX call to retrieve saved blocks.
                    BlocklyStorage.retrieveXml(window.location.hash.substring(1));
                }
                else if (loadOnce) {
                    // Language switching stores the blocks during the reload.
                    delete window.sessionStorage["loadOnceBlocks"];
                    var xml = Blockly.Xml.textToDom(loadOnce);
                    Blockly.Xml.domToWorkspace(_this.workspace, xml);
                }
                else if ("BlocklyStorage" in window) {
                    // Restore saved blocks in a separate thread so that subsequent
                    // initialization is not affected from a failed load.
                    window.setTimeout(BlocklyStorage.restoreBlocks, 0);
                }
            };
            this.getXmlBlocks = function () {
                var xmlElement = Blockly.Xml.workspaceToDom(_this.workspace);
                return Blockly.Xml.domToPrettyText(xmlElement);
            };
            this.getJsCode = function () {
                var code = Blockly.JavaScript.workspaceToCode(_this.workspace);
                return code;
            };
            this.getCSharpCode = function () {
                var code = Blockly.CSharp.workspaceToCode(_this.workspace);
                return code;
            };
            this.getWorkspace = function () {
                return _this.workspace;
            };
            this.init = function (blocklyElementId, blocklyToolboxElementId) {
                _this.blocklyElementId = blocklyElementId;
                _this.blocklyToolboxElementId = blocklyToolboxElementId;
                _this.angularBlocklyService.isLanguageScriptLoad().then(function () {
                    _this.initLanguage();
                    var rtl = _this.isRtl();
                    var toolbox = document.getElementById(_this.blocklyToolboxElementId);
                    _this.workspace = Blockly.inject(_this.blocklyElementId, {
                        grid: {
                            spacing: 25,
                            length: 3,
                            colour: "#ccc",
                            snap: true
                        },
                        media: _this.angularBlocklyOptionsProvider.getPath() + "media/",
                        rtl: rtl,
                        toolbox: toolbox,
                        zoom: {
                            controls: true,
                            wheel: true
                        }
                    });
                    // Add to reserved word list: Local variables in execution environment (runJS)
                    // and the infinite loop detection function.
                    Blockly.JavaScript.addReservedWords("code,timeouts,checkTimeout");
                    _this.workspace.addChangeListener(_this.onChange);
                });
            };
            /**
             * Initialize the page language.
             */
            this.initLanguage = function () {
                // get dynamicly categories detection
                var categoriesElements = angular.element(document).find("category");
                var categories = [];
                for (var index = 0, len = categoriesElements.length; index < len; index++) {
                    var element = categoriesElements[index];
                    categories.push(element.id);
                }
                for (var i = 0, cat = void 0; cat = categories[i]; i++) {
                    if (MSG[cat] || _this.scope.angularBlocklyOptions.customBlocks[cat].label["pl"]) {
                        var categoryLanguageLabel = MSG[cat] ? MSG[cat] : _this.scope.angularBlocklyOptions.customBlocks[cat].label["pl"];
                        var catElement = document.getElementById(cat);
                        if (catElement !== undefined && catElement !== null) {
                            catElement.setAttribute("name", categoryLanguageLabel);
                        }
                    }
                    else {
                        console.warn("Canot find translation for " + cat);
                    }
                }
                var textVars = document.getElementsByClassName("textVar");
                for (var i = 0, textVar = void 0; textVar = textVars[i]; i++) {
                    textVar.textContent = MSG["textVariable"];
                }
                var listVars = document.getElementsByClassName("listVar");
                for (var i = 0, listVar = void 0; listVar = listVars[i]; i++) {
                    listVar.textContent = MSG["listVariable"];
                }
            };
            /**
             * Execute the user's code.
             * Just a quick and dirty eval.  Catch infinite loops.
             */
            this.runJS = function () {
                Blockly.JavaScript.INFINITE_LOOP_TRAP = "  checkTimeout();\n";
                var timeouts = 0;
                var checkTimeout = function () {
                    if (timeouts++ > 1000000) {
                        throw MSG["timeout"];
                    }
                };
                var code = Blockly.JavaScript.workspaceToCode(_this.workspace);
                Blockly.JavaScript.INFINITE_LOOP_TRAP = null;
                try {
                    eval(code);
                }
                catch (e) {
                    alert(MSG["badCode"].replace("%1", e));
                }
            };
            /**
             * Discard all blocks from the workspace.
             */
            this.discard = function () {
                var count = _this.workspace.getAllBlocks().length;
                if (count < 2 ||
                    window.confirm(Blockly.Msg.DELETE_ALL_BLOCKS.replace("%1", count))) {
                    _this.workspace.clear();
                }
            };
            this.dispose = function () {
                _this.workspace.dispose();
                angular.element("#" + _this.scope.angularBlocklyTooltipId).remove();
            };
            this.onChange = function (event) {
                if (_this.scope.angularBlocklyOptions && _this.scope.angularBlocklyOptions.onChange) {
                    _this.timeoutService(function () {
                        _this.scope.angularBlocklyOptions.onChange(event);
                    }, 0);
                }
            };
            this.onRegisterApi = function () {
                if (_this.scope.angularBlocklyOptions && _this.scope.angularBlocklyOptions.xmlBlocks && _this.scope.angularBlocklyOptions.xmlBlocks.length > 0) {
                    _this.loadBlocks(_this.scope.angularBlocklyOptions.xmlBlocks);
                }
                // else {
                //     this.loadBlocks();
                // }
                if (_this.scope.hasOwnProperty("angularBlocklyOptions") && _this.scope["angularBlocklyOptions"].hasOwnProperty("onRegisterApi")) {
                    var api = {
                        getXmlBlocks: _this.getXmlBlocks,
                        getJsCode: _this.getJsCode,
                        getCSharpCode: _this.getCSharpCode,
                        getWorkspace: _this.getWorkspace,
                        loadBlocks: _this.loadBlocks,
                        dispose: _this.dispose
                    };
                    _this.scope.angularBlocklyOptions.onRegisterApi(api);
                }
            };
            this.scope = $scope;
            this.angularBlocklyService = angularBlocklyService;
            this.timeoutService = $timeout;
            this.angularBlocklyOptionsProvider = angularBlocklyOptionsProvider;
            this.initialized = false;
            var workspaceWatch = $scope.$watch("angularBlockyController.workspace", function (newValue, oldValue) {
                if (newValue) {
                    _this.initialized = true;
                    _this.onRegisterApi();
                    workspaceWatch();
                }
            });
        }
        AngularBlockyController.$inject = ["AngularBlockyService", "AngularBlocklyOptions", "$scope", "$timeout"];
        AngularBlockyController.LANGUAGE_NAME = {
            "en": "English",
            "pl": "Polski"
        };
        AngularBlockyController.LANGUAGE_RTL = ["ar", "fa", "he", "lki"];
        return AngularBlockyController;
    })();
    AngularBlocky.AngularBlockyController = AngularBlockyController;
})(AngularBlocky || (AngularBlocky = {}));

var AngularBlocky;
(function (AngularBlocky) {
    var AngularBlocklyOptionsProvider = (function () {
        function AngularBlocklyOptionsProvider() {
            var _this = this;
            this.path = "lib/blockly/";
            this.setPath = function (path) {
                _this.path = path;
            };
            this.$get = function () {
                return {
                    getPath: function () {
                        return _this.path;
                    }
                };
            };
        }
        return AngularBlocklyOptionsProvider;
    })();
    AngularBlocky.AngularBlocklyOptionsProvider = AngularBlocklyOptionsProvider;
})(AngularBlocky || (AngularBlocky = {}));

var AngularBlocky;
(function (AngularBlocky) {
    var AngularBlockyService = (function () {
        function AngularBlockyService($q, $locale, angularBlocklyOptionsProvider) {
            var _this = this;
            this.customBlocksName = [];
            this.isLanguageScriptLoad = function () { return _this.languageScriptLoaded; };
            /**
             * Set value for dropDown property
             */
            this.setDropDownList = function (blockly, newElementList, fieldName) {
                var dropdownField = blockly.getField(fieldName);
                dropdownField.menuGenerator_ = newElementList;
                var dropdownFieldOldValue = dropdownField.getValue();
                var newElement = dropdownField.menuGenerator_[0];
                if (dropdownFieldOldValue !== undefined && dropdownFieldOldValue !== null && dropdownFieldOldValue !== false) {
                    for (var index = 0; index < dropdownField.menuGenerator_.length; index++) {
                        var element = dropdownField.menuGenerator_[index];
                        if (element[1] === dropdownFieldOldValue) {
                            newElement = element;
                            break;
                        }
                    }
                }
                dropdownField.setText(newElement[0]); // set the actual text
                dropdownField.setValue(newElement[1]); // set the actual value
            };
            /**
             * Load data with Promise
             */
            this.setDataFromPromiseToInput = function (block, setDataFunction, dataFunctionParam, promise, promiseParam) {
                var defer = _this.qService.defer();
                block.setEditable(false);
                promise.apply(undefined, promiseParam).then(function (result) {
                    var dataFunctionAllParams = dataFunctionParam;
                    dataFunctionAllParams.unshift(result);
                    dataFunctionAllParams.unshift(block);
                    setDataFunction.apply(undefined, dataFunctionAllParams);
                    defer.resolve();
                }).catch(function () {
                    defer.reject();
                }).finally(function () {
                    block.setEditable(true);
                });
                return defer.promise;
            };
            this.getLang = function () {
                return _this.localeService.id.split("-")[0];
            };
            this.loadLanguageScript = function () {
                var head = document.getElementsByTagName("head")[0];
                var p1 = _this.qService.defer();
                var script1 = document.createElement("script");
                script1.setAttribute("src", _this.angularBlocklyOptionsProvider.getPath() + "msg/pl.js");
                head.appendChild(script1);
                script1.onload = function (e) {
                    p1.resolve();
                };
                var p2 = _this.qService.defer();
                var script2 = document.createElement("script");
                script2.setAttribute("src", _this.angularBlocklyOptionsProvider.getPath() + "msg/js/pl.js");
                script2.onload = function (e) {
                    p2.resolve();
                };
                head.appendChild(script2);
                return _this.qService.all([p1.promise, p2.promise]);
            };
            this.createModuleBlock = function () {
                Blockly.Blocks["controls_logical_and"] = {
                    init: function () {
                        this.appendDummyInput()
                            .appendField("Warunek logiczny")
                            .appendField(new Blockly.FieldDropdown([["i", "and"], ["lub", "or"]]), "logicOperator");
                        this.childrenName = "Condition";
                        this.appendValueInput(this.childrenName + "1").setCheck("Boolean");
                        this.appendValueInput(this.childrenName + "2").setCheck("Boolean");
                        this.setInputsInline(false);
                        this.setOutput(true, "Boolean");
                        this.setColour(Blockly.Blocks.logic.HUE);
                        this.setTooltip("Logical AND");
                        this.setMutator(new Blockly.Mutator(["controls_condition_block"]));
                        this.numberOfChildren = 2;
                    },
                    mutationToDom: function () {
                        if (!this.numberOfChildren) {
                            return null;
                        }
                        var container = document.createElement("mutation");
                        if (this.numberOfChildren) {
                            container.setAttribute("children", this.numberOfChildren);
                        }
                        return container;
                    },
                    domToMutation: function (xmlElement) {
                        this.numberOfChildren = parseInt(xmlElement.getAttribute("children"), 10) || 0;
                        for (var i = 3; i <= this.numberOfChildren; i++) {
                            this.appendValueInput(this.childrenName + i)
                                .setCheck("Boolean");
                        }
                    },
                    decompose: function (workspace) {
                        var containerBlock = Blockly.Block.obtain(workspace, "controls_and_tooltip");
                        containerBlock.initSvg();
                        var connection = containerBlock.getInput("STACK").connection;
                        for (var i = 1; i <= this.numberOfChildren; i++) {
                            var conditionBlock = Blockly.Block.obtain(workspace, "controls_condition_block");
                            conditionBlock.initSvg();
                            connection.connect(conditionBlock.previousConnection);
                            connection = conditionBlock.nextConnection;
                        }
                        return containerBlock;
                    },
                    compose: function (containerBlock) {
                        for (var i = this.numberOfChildren; i > 0; i--) {
                            this.removeInput(this.childrenName + i);
                        }
                        this.numberOfChildren = 0;
                        var clauseBlock = containerBlock.getInputTargetBlock("STACK");
                        while (clauseBlock) {
                            this.numberOfChildren++;
                            var andInput = this.appendValueInput(this.childrenName + this.numberOfChildren)
                                .setCheck("Boolean");
                            // .appendField('condition' + this.numberOfChildren);
                            if (clauseBlock.valueConnection_) {
                                andInput.connection.connect(clauseBlock.valueConnection_);
                            }
                            clauseBlock = clauseBlock.nextConnection &&
                                clauseBlock.nextConnection.targetBlock();
                        }
                    },
                    saveConnections: function (containerBlock) {
                        var clauseBlock = containerBlock.getInputTargetBlock("STACK");
                        var i = 1;
                        while (clauseBlock) {
                            var andInput = this.getInput(this.childrenName + i);
                            clauseBlock.valueConnection_ = andInput && andInput.connection.targetConnection;
                            i++;
                            clauseBlock = clauseBlock.nextConnection &&
                                clauseBlock.nextConnection.targetBlock();
                        }
                    }
                };
                Blockly.Blocks["controls_and_tooltip"] = {
                    init: function () {
                        this.setColour(Blockly.Blocks.logic.HUE);
                        this.appendDummyInput()
                            .appendField("Warunki");
                        this.appendStatementInput("STACK");
                        this.setTooltip("multiple and control");
                        this.contextMenu = false;
                    }
                };
                Blockly.Blocks["controls_condition_block"] = {
                    init: function () {
                        this.setColour(Blockly.Blocks.logic.HUE);
                        this.appendDummyInput()
                            .appendField("warunek");
                        this.setPreviousStatement(true);
                        this.setNextStatement(true);
                        this.setTooltip("conditionalStatement");
                        this.contextMenu = false;
                    }
                };
                Blockly.CSharp["controls_logical_and"] = function (block) {
                    var dropdownLogicOperator = block.getFieldValue("logicOperator");
                    var joinString = "";
                    switch (dropdownLogicOperator) {
                        case "or":
                            joinString = " || ";
                            break;
                        case "and":
                            joinString = " && ";
                            break;
                        default:
                            break;
                    }
                    var arrayOfCode = [];
                    for (var index = 1, len = block.numberOfChildren; index <= len; index++) {
                        var partCode = Blockly.CSharp.valueToCode(block, this.childrenName + index, Blockly.CSharp.ORDER_ATOMIC);
                        console.log("controls_logical_and", partCode);
                        if (partCode !== undefined && partCode !== null && partCode.length > 0) {
                            arrayOfCode.push(partCode);
                        }
                    }
                    var code = arrayOfCode.join(joinString);
                    console.log("controls_logical_and", code);
                    return [code, Blockly.CSharp.ORDER_NONE];
                };
                Blockly.JavaScript["controls_logical_and"] = function (block) {
                    var dropdownLogicOperator = block.getFieldValue("logicOperator");
                    var joinString = "";
                    switch (dropdownLogicOperator) {
                        case "or":
                            joinString = " || ";
                            break;
                        case "and":
                            joinString = " && ";
                            break;
                        default:
                            break;
                    }
                    var arrayOfCode = [];
                    for (var index = 1, len = block.numberOfChildren; index <= len; index++) {
                        var partCode = Blockly.JavaScript.valueToCode(block, this.childrenName + index, Blockly.JavaScript.ORDER_ATOMIC);
                        console.log("controls_logical_and", partCode);
                        if (partCode !== undefined && partCode !== null && partCode.length > 0) {
                            arrayOfCode.push(partCode);
                        }
                    }
                    var code = arrayOfCode.join(joinString);
                    console.log("controls_logical_and", code);
                    return [code, Blockly.JavaScript.ORDER_NONE];
                };
            };
            this.qService = $q;
            this.localeService = $locale;
            this.angularBlocklyOptionsProvider = angularBlocklyOptionsProvider;
            this.languageScriptLoaded = this.loadLanguageScript();
            this.createModuleBlock();
        }
        AngularBlockyService.$inject = ["$q", "$locale", "AngularBlocklyOptions"];
        AngularBlockyService.UsedGUIDs = [];
        AngularBlockyService.GenerateGuid = function () {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        };
        AngularBlockyService.CrateGuid = function () {
            var guid;
            while (guid === undefined || guid === null || AngularBlockyService.UsedGUIDs.indexOf(guid) !== -1) {
                guid = AngularBlockyService.GenerateGuid();
            }
            AngularBlockyService.UsedGUIDs.push(guid);
            return guid;
        };
        return AngularBlockyService;
    })();
    AngularBlocky.AngularBlockyService = AngularBlockyService;
})(AngularBlocky || (AngularBlocky = {}));

var AngularBlocky;
(function (AngularBlocky) {
    angular.module("angularBlockly", [])
        .directive("angularBlockly", AngularBlocky.AngularBlockyDirectiveFactory)
        .service("AngularBlockyService", AngularBlocky.AngularBlockyService)
        .provider("AngularBlocklyOptions", AngularBlocky.AngularBlocklyOptionsProvider)
        .run(["AngularBlockyService", function (angularBlocklyService) {
        }]);
})(AngularBlocky || (AngularBlocky = {}));
