declare namespace AngularBlocky {
}

declare var MSG: any;
declare var BlocklyStorage: any;
declare namespace AngularBlocky {
    interface IAngularBlocklyApi {
        getXmlBlocks: () => string;
        getJsCode: () => string;
        getCSharpCode: () => string;
        getWorkspace: () => Blockly.Workspace;
        loadBlocks: (defaultXml?: string) => void;
        dispose: () => void;
    }
    interface ILanguageLabel {
        [languageShot: string]: string;
    }
    interface IAngularBlocklyOptions {
        onRegisterApi?: (api: IAngularBlocklyApi) => void;
        onChange?: (event) => void;
        customBlocks?: {
            [catName: string]: {
                blocks: string[];
                color?: number;
                label?: ILanguageLabel;
            };
        };
        disableBlock?: {
            [catName: string]: string[];
        };
        xmlBlocks?: string;
        disableStorage?: boolean;
    }
    interface IAngularBlocklyScope extends ng.IScope {
        angularBlocklyOptions: IAngularBlocklyOptions;
        angularBlocklyTooltipId: string;
    }
    class AngularBlockyController implements IAngularBlocklyApi {
        static $inject: string[];
        static LANGUAGE_NAME: {
            "en": string;
            "pl": string;
        };
        static LANGUAGE_RTL: string[];
        private blocklyElementId;
        private blocklyToolboxElementId;
        private workspace;
        private angularBlocklyService;
        private timeoutService;
        private scope;
        private angularBlocklyOptionsProvider;
        private initialized;
        constructor(angularBlocklyService: AngularBlockyService, angularBlocklyOptionsProvider: IAngularBlocklyOptionsProvider, $scope: IAngularBlocklyScope, $timeout: ng.ITimeoutService);
        isRtl: () => boolean;
        /**
         * Load blocks saved on App Engine Storage or in session/local storage.
         * @param {string} defaultXml Text representation of default blocks.
         */
        loadBlocks: (defaultXml?: string) => void;
        getXmlBlocks: () => string;
        getJsCode: () => string;
        getCSharpCode: () => string;
        getWorkspace: () => Blockly.Workspace;
        init: (blocklyElementId: string, blocklyToolboxElementId: string) => void;
        /**
         * Initialize the page language.
         */
        initLanguage: () => void;
        /**
         * Execute the user's code.
         * Just a quick and dirty eval.  Catch infinite loops.
         */
        runJS: () => void;
        /**
         * Discard all blocks from the workspace.
         */
        discard: () => void;
        dispose: () => void;
        private onChange;
        private onRegisterApi;
    }
}

declare namespace AngularBlocky {
    class AngularBlockyDirective implements ng.IDirective {
        restrict: string;
        scope: {
            "angularBlocklyOptions": string;
        };
        controller: typeof AngularBlockyController;
        controllerAs: string;
        private httpService;
        private angularBlocklyOptions;
        constructor(http: ng.IHttpService, angularBlocklyOptionsProvider: IAngularBlocklyOptionsProvider);
        link: (scope: IAngularBlocklyScope, element: ng.IAugmentedJQuery, attrs: ng.IAttributes, controller: AngularBlockyController, transclude: ng.ITranscludeFunction) => void;
    }
    var AngularBlockyDirectiveFactory: (http: ng.IHttpService, angularBlocklyOptionsProvider: IAngularBlocklyOptionsProvider) => AngularBlockyDirective;
}


declare namespace AngularBlocky {
    interface IAngularBlocklyOptionsProvider {
        getPath: () => string;
    }
    class AngularBlocklyOptionsProvider implements ng.IServiceProvider {
        private path;
        setPath: (path: string) => void;
        $get: () => IAngularBlocklyOptionsProvider;
    }
}

declare namespace AngularBlocky {
    class AngularBlockyService {
        static $inject: string[];
        private static UsedGUIDs;
        private static GenerateGuid;
        static CrateGuid: () => string;
        private qService;
        private localeService;
        private angularBlocklyOptionsProvider;
        private languageScriptLoaded;
        private customBlocksName;
        constructor($q: ng.IQService, $locale: ng.ILocaleService, angularBlocklyOptionsProvider: IAngularBlocklyOptionsProvider);
        isLanguageScriptLoad: () => ng.IPromise<any>;
        /**
         * Set value for dropDown property
         */
        setDropDownList: (blockly: Blockly.Block, newElementList: any, fieldName: string) => void;
        /**
         * Load data with Promise
         */
        setDataFromPromiseToInput: (block: Blockly.Block, setDataFunction: (block: Blockly.Block, data: any, ...args: any[]) => any, dataFunctionParam: any[], promise: (...args: any[]) => ng.IPromise<any>, promiseParam: any[]) => ng.IPromise<any>;
        getLang: () => string;
        private loadLanguageScript;
        private createModuleBlock;
    }
}
