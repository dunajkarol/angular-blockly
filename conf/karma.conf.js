module.exports = function (config) {
  config.set({
    basePath: '../',
    urlRoot: 'base',
    frameworks: ["jasmine"],
    exclude: [
    ],
    files: [
      'dist/build/lib/angular/angular.js',
      'dist/build/lib/angular-tests/angular-mocks.js',
      "dist/build/ts/core/common/dictionary.js",
      "dist/build/ts/core/common/guid.js",
      "dist/build/ts/core/common/color.js",
      "dist/build/ts/core/common/storagedictionary.js",
      "dist/build/ts/core/common/stringdictionary.js",
      "dist/build/ts/core/common/inmemorycache.js",
      "dist/build/ts/core/common/localstoragecache.js",
      "dist/build/ts/core/coremodule.js",
        
      'tests/unit/commonfunction.js',
      'tests/unit/core/coreModuleTest.js',
      'tests/unit/core/global/colorTest.js',
    ],
    browsers: ['Chrome'],
    reporters: [
      'progress',
      'junit',
      'coverage'
    ],
    preprocessors: {
      'src/ts/**/*.ts': ['typescript'],
      'tests/**/*.ts': ['typescript'],
      'dist/build/scripts/**/*.js': ['coverage'],
      'dist/build/ts/**/*.js': ['coverage']
    },
    typescriptPreprocessor: {
      // options passed to the typescript compiler
      options: {
        sourceMap: false, // (optional) Generates corresponding .map file.
        target: 'ES6', // (optional) Specify ECMAScript target version: 'ES3' (default), or 'ES5'
        module: 'amd', // (optional) Specify module code generation: 'commonjs' or 'amd'
        noImplicitAny: true, // (optional) Warn on expressions and declarations with an implied 'any' type.
        noResolve: true, // (optional) Skip resolution and preprocessing.
        removeComments: true // (optional) Do not emit comments to output.
      },
      // transforming the filenames
      transformPath: function (path) {
        return path.replace(/\.ts$/, '.js');
      }
    },
    junitReporter: {
      outputFile: 'dist/test_out/unit/unit.xml',
      suite: 'unit'
    },
    customLaunchers: {
      Chrome_without_security: {
        base: 'Chrome',
        flags: ['--disable-web-security']
      }
    },
    coverageReporter: {
      type: 'html',
      dir: 'dist/coverage/'
    },
    singleRun: true
  });
};