
/// <reference path="common.d.ts" />

angular
.module("App", ["ngAnimate", "ngAria", "ngMaterial", "ngRoute", "angularBlockly", "Exdata"])
.config(["AngularBlocklyOptionsProvider", (angularBlocklyProvider: AngularBlocky.AngularBlocklyOptionsProvider) => {
    angularBlocklyProvider.setPath("assets/");
}]);