﻿/// <reference path="../common.d.ts" />

namespace AngularBlocky {
    angular.module("angularBlockly", [])
    .directive("angularBlockly", AngularBlockyDirectiveFactory)
    .service("AngularBlockyService", AngularBlockyService)
    .provider("AngularBlocklyOptions", AngularBlocklyOptionsProvider) 
    .run(["AngularBlockyService", (angularBlocklyService: AngularBlockyService) => {

    }]);

}

