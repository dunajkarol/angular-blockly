/// <reference path="../../../../common.d.ts" />
declare namespace AngularBlocky {
    interface IAngularBlocklyOptionsProvider {
        getPath: () => string;
    }
    class AngularBlocklyOptionsProvider implements ng.IServiceProvider {
        private path;
        setPath: (path: string) => void;
        $get: () => IAngularBlocklyOptionsProvider;
    }
}
