/// <reference path="../../../../common.d.ts" />
declare namespace AngularBlocky {
    class AngularBlockyService {
        static $inject: string[];
        private static UsedGUIDs;
        private static GenerateGuid;
        static CrateGuid: () => string;
        private qService;
        private localeService;
        private angularBlocklyOptionsProvider;
        private languageScriptLoaded;
        private customBlocksName;
        constructor($q: ng.IQService, $locale: ng.ILocaleService, angularBlocklyOptionsProvider: IAngularBlocklyOptionsProvider);
        isLanguageScriptLoad: () => ng.IPromise<any>;
        /**
         * Set value for dropDown property
         */
        setDropDownList: (blockly: Blockly.Block, newElementList: any, fieldName: string) => void;
        /**
         * Load data with Promise
         */
        setDataFromPromiseToInput: (block: Blockly.Block, setDataFunction: (block: Blockly.Block, data: any, ...args: any[]) => any, dataFunctionParam: any[], promise: (...args: any[]) => ng.IPromise<any>, promiseParam: any[]) => ng.IPromise<any>;
        getLang: () => string;
        private loadLanguageScript;
        private createModuleBlock;
    }
}
