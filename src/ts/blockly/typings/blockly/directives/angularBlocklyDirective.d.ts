/// <reference path="../../../../common.d.ts" />
declare namespace AngularBlocky {
    class AngularBlockyDirective implements ng.IDirective {
        restrict: string;
        scope: {
            "angularBlocklyOptions": string;
        };
        controller: typeof AngularBlockyController;
        controllerAs: string;
        private httpService;
        private angularBlocklyOptions;
        constructor(http: ng.IHttpService, angularBlocklyOptionsProvider: IAngularBlocklyOptionsProvider);
        link: (scope: IAngularBlocklyScope, element: ng.IAugmentedJQuery, attrs: ng.IAttributes, controller: AngularBlockyController, transclude: ng.ITranscludeFunction) => void;
    }
    var AngularBlockyDirectiveFactory: (http: ng.IHttpService, angularBlocklyOptionsProvider: IAngularBlocklyOptionsProvider) => AngularBlockyDirective;
}
