/// <reference path="../../../../common.d.ts" />
declare var MSG: any;
declare var BlocklyStorage: any;
declare namespace AngularBlocky {
    interface IAngularBlocklyApi {
        getXmlBlocks: () => string;
        getJsCode: () => string;
        getCSharpCode: () => string;
        getWorkspace: () => Blockly.Workspace;
        loadBlocks: (defaultXml?: string) => void;
        dispose: () => void;
    }
    interface ILanguageLabel {
        [languageShot: string]: string;
    }
    interface IAngularBlocklyOptions {
        onRegisterApi?: (api: IAngularBlocklyApi) => void;
        onChange?: (event) => void;
        customBlocks?: {
            [catName: string]: {
                blocks: string[];
                color?: number;
                label?: ILanguageLabel;
            };
        };
        disableBlock?: {
            [catName: string]: string[];
        };
        xmlBlocks?: string;
        disableStorage?: boolean;
    }
    interface IAngularBlocklyScope extends ng.IScope {
        angularBlocklyOptions: IAngularBlocklyOptions;
        angularBlocklyTooltipId: string;
    }
    class AngularBlockyController implements IAngularBlocklyApi {
        static $inject: string[];
        static LANGUAGE_NAME: {
            "en": string;
            "pl": string;
        };
        static LANGUAGE_RTL: string[];
        private blocklyElementId;
        private blocklyToolboxElementId;
        private workspace;
        private angularBlocklyService;
        private timeoutService;
        private scope;
        private angularBlocklyOptionsProvider;
        private initialized;
        constructor(angularBlocklyService: AngularBlockyService, angularBlocklyOptionsProvider: IAngularBlocklyOptionsProvider, $scope: IAngularBlocklyScope, $timeout: ng.ITimeoutService);
        isRtl: () => boolean;
        /**
         * Load blocks saved on App Engine Storage or in session/local storage.
         * @param {string} defaultXml Text representation of default blocks.
         */
        loadBlocks: (defaultXml?: string) => void;
        getXmlBlocks: () => string;
        getJsCode: () => string;
        getCSharpCode: () => string;
        getWorkspace: () => Blockly.Workspace;
        init: (blocklyElementId: string, blocklyToolboxElementId: string) => void;
        /**
         * Initialize the page language.
         */
        initLanguage: () => void;
        /**
         * Execute the user's code.
         * Just a quick and dirty eval.  Catch infinite loops.
         */
        runJS: () => void;
        /**
         * Discard all blocks from the workspace.
         */
        discard: () => void;
        dispose: () => void;
        private onChange;
        private onRegisterApi;
    }
}
