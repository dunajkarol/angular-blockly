/// <reference path="../../common.d.ts" />

namespace AngularBlocky {

    export interface IAngularBlocklyOptionsProvider {
        getPath: () => string;
    }

    export class AngularBlocklyOptionsProvider implements ng.IServiceProvider {
        private path: string = "lib/blockly/";

        public setPath = (path: string) => {
            this.path = path;
        }

        public $get = (): IAngularBlocklyOptionsProvider  => {
            return {
                getPath: (): string => {
                    return this.path;
                }
            }
        }
    }
}