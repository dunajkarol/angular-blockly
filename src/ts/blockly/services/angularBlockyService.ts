/// <reference path="../../common.d.ts" />

namespace AngularBlocky {
    export class AngularBlockyService {
        static $inject = ["$q", "$locale", "AngularBlocklyOptions"];
        private static UsedGUIDs = [];

        private static GenerateGuid = (): string => {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }

        static CrateGuid = (): string => {
            let guid: string;
            while (guid === undefined || guid === null || AngularBlockyService.UsedGUIDs.indexOf(guid) !== -1) {
                guid = AngularBlockyService.GenerateGuid();
            }

            AngularBlockyService.UsedGUIDs.push(guid);

            return guid;
        }

        private qService: ng.IQService;
        private localeService: ng.ILocaleService;
        private angularBlocklyOptionsProvider: IAngularBlocklyOptionsProvider;

        private languageScriptLoaded: ng.IPromise<any>;

        private customBlocksName = [];

        constructor($q: ng.IQService, $locale: ng.ILocaleService, angularBlocklyOptionsProvider: IAngularBlocklyOptionsProvider) {
            this.qService = $q;
            this.localeService = $locale;
            this.angularBlocklyOptionsProvider = angularBlocklyOptionsProvider;
            this.languageScriptLoaded = this.loadLanguageScript();
            this.createModuleBlock();
        }

        public isLanguageScriptLoad = () => this.languageScriptLoaded;

        /**
         * Set value for dropDown property
         */
        public setDropDownList = (blockly: Blockly.Block, newElementList: any, fieldName: string) => {
            let dropdownField: any = blockly.getField(fieldName);
            dropdownField.menuGenerator_ = newElementList;
            let dropdownFieldOldValue = dropdownField.getValue();
            let newElement = dropdownField.menuGenerator_[0];

            if (dropdownFieldOldValue !== undefined && dropdownFieldOldValue !== null && dropdownFieldOldValue !== false) {
                for (let index = 0; index < dropdownField.menuGenerator_.length; index++) {
                    let element: string[] = dropdownField.menuGenerator_[index];
                    if (element[1] === dropdownFieldOldValue) {
                        newElement = element;
                        break;
                    }
                }
            }

            dropdownField.setText(newElement[0]); // set the actual text
            dropdownField.setValue(newElement[1]); // set the actual value
        }


        /**
         * Load data with Promise
         */
        public setDataFromPromiseToInput = (
            block: Blockly.Block,
            setDataFunction: (block: Blockly.Block, data: any, ...args: any[]) => any,
            dataFunctionParam: any[],
            promise: (...args: any[]) => ng.IPromise<any>,
            promiseParam: any[]): ng.IPromise<any> => {
            
            let defer = this.qService.defer();
            block.setEditable(false);
            promise.apply(undefined, promiseParam).then((result) => {

                let dataFunctionAllParams = dataFunctionParam;
                dataFunctionAllParams.unshift(result);
                dataFunctionAllParams.unshift(block);

                setDataFunction.apply(undefined, dataFunctionAllParams);
                defer.resolve();
            }).catch(() => {
                
                defer.reject();
            }).finally(() => {
                block.setEditable(true);
            });

            return defer.promise;
        }

        public getLang = (): string => {
            return this.localeService.id.split("-")[0];
        }

        private loadLanguageScript = (): ng.IPromise<any> => {


            let head = document.getElementsByTagName("head")[0];

            let p1: ng.IDeferred<any> = this.qService.defer();

            let script1 = document.createElement("script");
            script1.setAttribute("src", this.angularBlocklyOptionsProvider.getPath() + "msg/pl.js");
            head.appendChild(script1);
            script1.onload = (e) => {
                p1.resolve();
            };

            let p2: ng.IDeferred<any> = this.qService.defer();
            let script2 = document.createElement("script");
            script2.setAttribute("src", this.angularBlocklyOptionsProvider.getPath() + "msg/js/pl.js");
            script2.onload = (e) => {
                p2.resolve();
            };

            head.appendChild(script2);

            return this.qService.all([p1.promise, p2.promise]);
        }

        private createModuleBlock = () => {

            Blockly.Blocks["controls_logical_and"] = {
                init: function () {
                    this.appendDummyInput()
                        .appendField("Warunek logiczny")
                        .appendField(new Blockly.FieldDropdown([["i", "and"], ["lub", "or"]]), "logicOperator");
                    this.childrenName = "Condition";
                    this.appendValueInput(this.childrenName + "1").setCheck("Boolean");
                    this.appendValueInput(this.childrenName + "2").setCheck("Boolean");
                    this.setInputsInline(false);
                    this.setOutput(true, "Boolean");
                    this.setColour(Blockly.Blocks.logic.HUE);
                    this.setTooltip("Logical AND");
                    this.setMutator(new Blockly.Mutator(["controls_condition_block"]));
                    this.numberOfChildren = 2;
                },
                mutationToDom: function () {
                    if (!this.numberOfChildren) {
                        return null;
                    }
                    let container = document.createElement("mutation");
                    if (this.numberOfChildren) {
                        container.setAttribute("children", this.numberOfChildren);
                    }
                    return container;
                },
                domToMutation: function (xmlElement) {
                    this.numberOfChildren = parseInt(xmlElement.getAttribute("children"), 10) || 0;
                    for (let i = 3; i <= this.numberOfChildren; i++) {
                        this.appendValueInput(this.childrenName + i)
                            .setCheck("Boolean");
                    }
                },
                decompose: function (workspace) {
                    let containerBlock = Blockly.Block.obtain(workspace, "controls_and_tooltip");
                    containerBlock.initSvg();
                    let connection = containerBlock.getInput("STACK").connection;
                    for (let i = 1; i <= this.numberOfChildren; i++) {
                        let conditionBlock = Blockly.Block.obtain(workspace, "controls_condition_block");
                        conditionBlock.initSvg();
                        connection.connect(conditionBlock.previousConnection);
                        connection = conditionBlock.nextConnection;
                    }
                    return containerBlock;
                },
                compose: function (containerBlock) {
                    for (let i = this.numberOfChildren; i > 0; i--) {
                        this.removeInput(this.childrenName + i);
                    }
                    this.numberOfChildren = 0;
                    let clauseBlock = containerBlock.getInputTargetBlock("STACK");
                    while (clauseBlock) {
                        this.numberOfChildren++;
                        let andInput = this.appendValueInput(this.childrenName + this.numberOfChildren)
                            .setCheck("Boolean");
                        // .appendField('condition' + this.numberOfChildren);
                        if (clauseBlock.valueConnection_) {
                            andInput.connection.connect(clauseBlock.valueConnection_);
                        }
                        clauseBlock = clauseBlock.nextConnection &&
                            clauseBlock.nextConnection.targetBlock();
                    }
                },
                saveConnections: function (containerBlock) {
                    let clauseBlock = containerBlock.getInputTargetBlock("STACK");
                    let i = 1;
                    while (clauseBlock) {
                        let andInput = this.getInput(this.childrenName + i);
                        clauseBlock.valueConnection_ = andInput && andInput.connection.targetConnection;
                        i++;
                        clauseBlock = clauseBlock.nextConnection &&
                            clauseBlock.nextConnection.targetBlock();
                    }
                }
            };

            Blockly.Blocks["controls_and_tooltip"] = {
                init: function () {
                    this.setColour(Blockly.Blocks.logic.HUE);
                    this.appendDummyInput()
                        .appendField("Warunki");
                    this.appendStatementInput("STACK");
                    this.setTooltip("multiple and control");
                    this.contextMenu = false;
                }
            };

            Blockly.Blocks["controls_condition_block"] = {
                init: function () {
                    this.setColour(Blockly.Blocks.logic.HUE);
                    this.appendDummyInput()
                        .appendField("warunek");
                    this.setPreviousStatement(true);
                    this.setNextStatement(true);
                    this.setTooltip("conditionalStatement");
                    this.contextMenu = false;
                }
            };

            Blockly.CSharp["controls_logical_and"] = function (block) {
                let dropdownLogicOperator = block.getFieldValue("logicOperator");
                let joinString = "";
                switch (dropdownLogicOperator) {
                    case "or":
                        joinString = " || ";
                        break;
                    case "and":
                        joinString = " && ";

                        break;

                    default:
                        break;
                }

                let arrayOfCode = [];
                for (let index = 1, len = block.numberOfChildren; index <= len; index++) {
                    let partCode = Blockly.CSharp.valueToCode(block, this.childrenName + index, Blockly.CSharp.ORDER_ATOMIC);

                    console.log("controls_logical_and", partCode);
                    if (partCode !== undefined && partCode !== null && partCode.length > 0) {
                        arrayOfCode.push(partCode);
                    }
                }

                let code = arrayOfCode.join(joinString);

                console.log("controls_logical_and", code);
                return [code, Blockly.CSharp.ORDER_NONE];
            };

            Blockly.JavaScript["controls_logical_and"] = function (block) {
                let dropdownLogicOperator = block.getFieldValue("logicOperator");
                let joinString = "";
                switch (dropdownLogicOperator) {
                    case "or":
                        joinString = " || ";
                        break;
                    case "and":
                        joinString = " && ";

                        break;

                    default:
                        break;
                }

                let arrayOfCode = [];
                for (let index = 1, len = block.numberOfChildren; index <= len; index++) {
                    let partCode = Blockly.JavaScript.valueToCode(block, this.childrenName + index, Blockly.JavaScript.ORDER_ATOMIC);

                    console.log("controls_logical_and", partCode);
                    if (partCode !== undefined && partCode !== null && partCode.length > 0) {
                        arrayOfCode.push(partCode);
                    }
                }

                let code = arrayOfCode.join(joinString);

                console.log("controls_logical_and", code);
                return [code, Blockly.JavaScript.ORDER_NONE];
            };
        }

    }
}