/// <reference path="../../common.d.ts" />

declare var MSG: any;
declare var BlocklyStorage: any;

namespace AngularBlocky {

    export interface IAngularBlocklyApi {
        getXmlBlocks: () => string;
        getJsCode: () => string;
        getCSharpCode: () => string;
        getWorkspace: () => Blockly.Workspace,
        loadBlocks: (defaultXml?: string) => void;
        dispose: () => void;
    }

    export interface ILanguageLabel {
        [languageShot: string]: string;
    }

    export interface IAngularBlocklyOptions {
        onRegisterApi?: (api: IAngularBlocklyApi) => void;
        onChange?: (event) => void;
        customBlocks?: {
            [catName: string]: {
                blocks: string[],
                color?: number,
                label?: ILanguageLabel
            };
        };
        disableBlock?: { [catName: string]: string[] };
        xmlBlocks?: string;
        disableStorage?: boolean;

    }

    export interface IAngularBlocklyScope extends ng.IScope {
        angularBlocklyOptions: IAngularBlocklyOptions;
        angularBlocklyTooltipId: string;
    }
    export class AngularBlockyController implements IAngularBlocklyApi {
        static $inject = ["AngularBlockyService", "AngularBlocklyOptions", "$scope", "$timeout"];
        static LANGUAGE_NAME = {
            "en": "English",
            "pl": "Polski"
        };
        static LANGUAGE_RTL = ["ar", "fa", "he", "lki"];

        private blocklyElementId: string;
        private blocklyToolboxElementId: string;
        private workspace: Blockly.Workspace;
        private angularBlocklyService: AngularBlockyService;
        private timeoutService: ng.ITimeoutService;
        private scope: IAngularBlocklyScope;
        private angularBlocklyOptionsProvider: IAngularBlocklyOptionsProvider;
        private initialized: boolean


        constructor(angularBlocklyService: AngularBlockyService, angularBlocklyOptionsProvider: IAngularBlocklyOptionsProvider, $scope: IAngularBlocklyScope, $timeout: ng.ITimeoutService) {
            this.scope = $scope;
            this.angularBlocklyService = angularBlocklyService;
            this.timeoutService = $timeout;
            this.angularBlocklyOptionsProvider = angularBlocklyOptionsProvider;
            this.initialized = false;
            let workspaceWatch = $scope.$watch("angularBlockyController.workspace", (newValue, oldValue) => {
                if (newValue) {
                    this.initialized = true;
                    this.onRegisterApi();
                    workspaceWatch();
                }
            });

        }

        public isRtl = () => {
            return AngularBlockyController.LANGUAGE_RTL.indexOf(this.angularBlocklyService.getLang()) !== -1;
        };

        /**
         * Load blocks saved on App Engine Storage or in session/local storage.
         * @param {string} defaultXml Text representation of default blocks.
         */
        public loadBlocks = (defaultXml?: string) => {
            let loadOnce: any;
            if (!this.initialized) {
                console.warn("Blokly nie zainicjalizowane");
                return;
            }
            try {
                loadOnce = window.sessionStorage["loadOnceBlocks"];
            } catch (e) {
                // Firefox sometimes throws a SecurityError when accessing sessionStorage.
                // Restarting Firefox fixes this, so it looks like a bug.
                loadOnce = null;
            }
            if (defaultXml) {
                // Load the editor with default starting blocks.
                let xml = Blockly.Xml.textToDom(defaultXml);
                Blockly.Xml.domToWorkspace(this.workspace, xml);
            } else if ("BlocklyStorage" in window && window.location.hash.length > 1) {
                // An href with #key trigers an AJAX call to retrieve saved blocks.
                BlocklyStorage.retrieveXml(window.location.hash.substring(1));
            } else if (loadOnce) {
                // Language switching stores the blocks during the reload.
                delete window.sessionStorage["loadOnceBlocks"];
                let xml = Blockly.Xml.textToDom(loadOnce);
                Blockly.Xml.domToWorkspace(this.workspace, xml);
            } else if ("BlocklyStorage" in window) {
                // Restore saved blocks in a separate thread so that subsequent
                // initialization is not affected from a failed load.
                window.setTimeout(BlocklyStorage.restoreBlocks, 0);
            }
        };

        public getXmlBlocks = (): string => {
            let xmlElement = Blockly.Xml.workspaceToDom(this.workspace);
            return Blockly.Xml.domToPrettyText(xmlElement);
        }

        public getJsCode = (): string => {
            let code: string = Blockly.JavaScript.workspaceToCode(this.workspace);
            return code;
        }

        public getCSharpCode = (): string => {
            let code: string = Blockly.CSharp.workspaceToCode(this.workspace);
            return code;
        }

        public getWorkspace = (): Blockly.Workspace => {
            return this.workspace;
        }

        public init = (blocklyElementId: string, blocklyToolboxElementId: string) => {
            this.blocklyElementId = blocklyElementId;
            this.blocklyToolboxElementId = blocklyToolboxElementId;

            this.angularBlocklyService.isLanguageScriptLoad().then(() => {
                this.initLanguage();
                let rtl = this.isRtl();


                let toolbox = document.getElementById(this.blocklyToolboxElementId);
                this.workspace = Blockly.inject(this.blocklyElementId,
                    {
                        grid:
                        {
                            spacing: 25,
                            length: 3,
                            colour: "#ccc",
                            snap: true
                        },
                        media: this.angularBlocklyOptionsProvider.getPath() + "media/",
                        rtl: rtl,
                        toolbox: toolbox,
                        zoom:
                        {
                            controls: true,
                            wheel: true
                        }
                    });

                // Add to reserved word list: Local variables in execution environment (runJS)
                // and the infinite loop detection function.
                Blockly.JavaScript.addReservedWords("code,timeouts,checkTimeout");
                (<Blockly.WorkspaceSvg>this.workspace).addChangeListener(this.onChange);

            });
        };

        /**
         * Initialize the page language.
         */
        public initLanguage = () => {
            // get dynamicly categories detection
            let categoriesElements = angular.element(document).find("category");

            let categories: string[] = [];

            for (let index = 0, len = categoriesElements.length; index < len; index++) {
                let element = categoriesElements[index];
                categories.push(element.id);
            }

            for (let i = 0, cat; cat = categories[i]; i++) {
                if (MSG[cat] || this.scope.angularBlocklyOptions.customBlocks[cat].label["pl"]) {
                    let categoryLanguageLabel = MSG[cat] ? MSG[cat] : this.scope.angularBlocklyOptions.customBlocks[cat].label["pl"];

                    let catElement = document.getElementById(cat);
                    if (catElement !== undefined && catElement !== null) {
                        catElement.setAttribute("name", categoryLanguageLabel);
                    }
                } else {
                    console.warn(`Canot find translation for ${cat}`);
                }

            }
            let textVars = document.getElementsByClassName("textVar");
            for (let i = 0, textVar; textVar = textVars[i]; i++) {
                textVar.textContent = MSG["textVariable"];
            }
            let listVars = document.getElementsByClassName("listVar");
            for (let i = 0, listVar; listVar = listVars[i]; i++) {
                listVar.textContent = MSG["listVariable"];
            }
        };

        /**
         * Execute the user's code.
         * Just a quick and dirty eval.  Catch infinite loops.
         */
        public runJS = () => {
            Blockly.JavaScript.INFINITE_LOOP_TRAP = "  checkTimeout();\n";
            let timeouts = 0;
            let checkTimeout = () => {
                if (timeouts++ > 1000000) {
                    throw MSG["timeout"];
                }
            };
            let code = Blockly.JavaScript.workspaceToCode(this.workspace);
            Blockly.JavaScript.INFINITE_LOOP_TRAP = null;
            try {
                eval(code);
            } catch (e) {
                alert(MSG["badCode"].replace("%1", e));
            }
        };

        /**
         * Discard all blocks from the workspace.
         */
        public discard = () => {
            let count = this.workspace.getAllBlocks().length;
            if (count < 2 ||
                window.confirm(Blockly.Msg.DELETE_ALL_BLOCKS.replace("%1", count))) {
                this.workspace.clear();
            }
        };

        public dispose = () => {
            this.workspace.dispose();
            angular.element(`#${this.scope.angularBlocklyTooltipId}`).remove();
        }

        private onChange = (event) => {
            if (this.scope.angularBlocklyOptions && this.scope.angularBlocklyOptions.onChange) {

                this.timeoutService(() => {
                    this.scope.angularBlocklyOptions.onChange(event);
                }, 0);
            }
        }

        private onRegisterApi = () => {

            if (this.scope.angularBlocklyOptions && this.scope.angularBlocklyOptions.xmlBlocks && this.scope.angularBlocklyOptions.xmlBlocks.length > 0) {
                this.loadBlocks(this.scope.angularBlocklyOptions.xmlBlocks);
            } 
            // else {
            //     this.loadBlocks();
            // }

            if (this.scope.hasOwnProperty("angularBlocklyOptions") && this.scope["angularBlocklyOptions"].hasOwnProperty("onRegisterApi")) {
                let api: IAngularBlocklyApi = {
                    getXmlBlocks: this.getXmlBlocks,
                    getJsCode: this.getJsCode,
                    getCSharpCode: this.getCSharpCode,
                    getWorkspace: this.getWorkspace,
                    loadBlocks: this.loadBlocks,
                    dispose: this.dispose
                };

                this.scope.angularBlocklyOptions.onRegisterApi(api);
            }
        }

    }
}