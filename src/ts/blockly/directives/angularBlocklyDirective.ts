/// <reference path="../../common.d.ts" />

namespace AngularBlocky {
    export class AngularBlockyDirective implements ng.IDirective {

        public restrict = "A";
        // public transclude = true;
        // public replace= false;
        public scope = {
            "angularBlocklyOptions": "=",
            // "treeFunctions": "="
        };
        public controller = AngularBlockyController;
        public controllerAs = "angularBlockyController";

        private httpService: ng.IHttpService;
        private angularBlocklyOptions: IAngularBlocklyOptionsProvider;

        constructor(
            http: ng.IHttpService,
            angularBlocklyOptionsProvider: IAngularBlocklyOptionsProvider
        ) {
            this.httpService = http;
            this.angularBlocklyOptions = angularBlocklyOptionsProvider;
        }

        public link = (scope: IAngularBlocklyScope, element: ng.IAugmentedJQuery, attrs: ng.IAttributes, controller: AngularBlockyController, transclude: ng.ITranscludeFunction): void => {

            let id: string;
            let guid: string;

            element.addClass("angular-blockly");

            if (!attrs.hasOwnProperty("id")) {
                guid = AngularBlockyService.CrateGuid();
                id = "angular-blockly" + guid;
                element.attr("id", id);
            } else {
                id = attrs["id"];
            }

            this.httpService.get(this.angularBlocklyOptions.getPath() + "files/defaultToolbox.xml")
                .then((result) => {
                    let $xml = angular.element(result.data);
                    let toolboxId: string;
                    if (guid !== undefined && guid !== null && guid.length > 0) {
                        toolboxId = "angular-toolbox-" + guid;
                    } else {
                        toolboxId = id + "-toolbox";
                    }



                    scope.angularBlocklyTooltipId = toolboxId;

                    $xml.attr("id", toolboxId);

                    // remove tool from toolbox
                    if (scope.angularBlocklyOptions && scope.angularBlocklyOptions.disableBlock && Object.keys(scope.angularBlocklyOptions.disableBlock).length > 0) {
                        let keys = Object.keys(scope.angularBlocklyOptions.disableBlock);
                        for (let i = 0; i < keys.length; i++) {
                            let categoryName = keys[i];
                            let category = scope.angularBlocklyOptions.disableBlock[categoryName];
                            if (category && category.length > 0) {
                                for (let j = 0; j < category.length; j++) {
                                    let blockName = category[j];

                                    angular.element($xml[0].querySelector(`category#${categoryName} block[type='${blockName}']`)).remove();
                                }
                            } else if (category === null) {
                                angular.element($xml[0].querySelector(`category#${categoryName}`)).remove();
                            }
                        }
                    }

                    if (scope.angularBlocklyOptions && scope.angularBlocklyOptions.customBlocks && Object.keys(scope.angularBlocklyOptions.customBlocks).length > 0) {
                        let keys = Object.keys(scope.angularBlocklyOptions.customBlocks);
                        for (let i = 0; i < keys.length; i++) {
                            let categoryName = keys[i];
                            let category = scope.angularBlocklyOptions.customBlocks[categoryName];
                            if (category && category.blocks.length > 0) {
                                let $category = $xml.find(`category#${categoryName}`);
                                if ($category.length === 0) {
                                    $category = angular.element(`<category id=\"${categoryName}\"></category>`);
                                }
                                if (category.color || category.color === 0) {
                                    $category.attr("colour", category.color);
                                }
                                
                                for (let j = 0; j < category.blocks.length; j++) {
                                    let blockName = category.blocks[j];

                                    $category.append("<block type=\"" + blockName + "\"></block>");
                                }
                                $xml.append($category);
                            }
                        }
                    }


                    let body = document.getElementsByTagName("body")[0];
                    angular.element(body).append($xml);

                    controller.init(id, toolboxId);
                });



            // ustaw w kontrolerze id 
            // inicjalizuj blockly



        }
    }

    export var AngularBlockyDirectiveFactory = (
        http: ng.IHttpService,
        angularBlocklyOptionsProvider: IAngularBlocklyOptionsProvider
    ) => {
        return new AngularBlockyDirective(
            http, angularBlocklyOptionsProvider
        );
    };

    AngularBlockyDirectiveFactory.$inject = ["$http", "AngularBlocklyOptions"];
}