/// <reference path="../../common.d.ts" />

namespace Exdata {
    export class TestController {
        public blocklyOptions: AngularBlocky.IAngularBlocklyOptions;
        public blocklyApi: AngularBlocky.IAngularBlocklyApi;
        public xmlCode: string;
        public jsCode: string;
        public cSharpCode: string;
        constructor() {
            console.log("");
            this.xmlCode = "";
            this.jsCode = "";
            this.blocklyOptions = {
                onRegisterApi: (api) => {
                    this.blocklyApi = api;
                },
                onChange: (event) => {
                    this.refreshXmlFromDirective();
                },
                disableBlock: {
                    "catColour": null
                },
                customBlocks: {
                    "catMeasuringPoint": {
                        blocks: ["dayincrease"],
                        color: 20
                    },
                    "catAction": {
                        blocks: ["dayincrease"],
                        color: 10,
                        label: {pl: "Akcje"}
                    }
                }
            };
        }

        public refreshXmlFromDirective = () => {
            this.xmlCode = this.blocklyApi.getXmlBlocks();
            this.jsCode = this.blocklyApi.getJsCode();
            this.cSharpCode = this.blocklyApi.getCSharpCode();
        }
    }
}