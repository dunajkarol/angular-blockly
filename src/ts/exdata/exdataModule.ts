﻿/// <reference path="../common.d.ts" />

namespace Exdata {
    angular.module("Exdata", [])
    .controller("TestController", TestController)
    .service(SimpleDataService.Name, SimpleDataService)
    .service(CustomBlockyService.Name, CustomBlockyService)
    .run([CustomBlockyService.Name, (customBlockyService: CustomBlockyService) => {

    }]);

}