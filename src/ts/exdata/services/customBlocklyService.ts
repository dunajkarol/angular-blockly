/// <reference path="../../common.d.ts" />

namespace Exdata {
    export class CustomBlockyService {
        static $inject = [SimpleDataService.Name, "AngularBlockyService"];

        static Name = "CustomBlockyService";

        private simpleDataService: SimpleDataService;
        private blockyService: AngularBlocky.AngularBlockyService;

        constructor(simpleDataService: SimpleDataService, angularBlockyService: AngularBlocky.AngularBlockyService) {
            this.simpleDataService = simpleDataService;
            this.blockyService = angularBlockyService;
            this.createBlockly();
        }

        private createBlockly = () => {
            let dataService = this.simpleDataService;
            let blocklyService = this.blockyService;

            Blockly.Blocks["dayincrease"] = {
                init: function () {
                    let self: Blockly.Block = this;


                    let onChangeFunctionsDropDown = function (newValue) {
                        blocklyService.setDataFromPromiseToInput(self, blocklyService.setDropDownList, ["functionsParameter"], dataService.getFunctionsParameterPromise, [newValue]);
                    }
                    this.appendDummyInput()
                        .setAlign(Blockly.ALIGN_CENTRE)
                        .appendField("Przyrost Dobowy");

                    let functionsDropDown = new Blockly.FieldDropdown([[]], onChangeFunctionsDropDown);

                    this.appendDummyInput()
                        .appendField("Funkcja")
                        .appendField(functionsDropDown, "functions");

                    let promise = blocklyService.setDataFromPromiseToInput(self, blocklyService.setDropDownList, ["functions"], dataService.getFunctionsPromise, [1]);


                    let functionsParameterDropDown = new Blockly.FieldDropdown([[]]);

                    this.appendDummyInput()
                        .appendField("Parametr")
                        .appendField(functionsParameterDropDown, "functionsParameter");
                        promise.then(() => {
                            blocklyService.setDataFromPromiseToInput(self, blocklyService.setDropDownList, ["functionsParameter"], dataService.getFunctionsParameterPromise, [functionsDropDown.getValue()]);
                        })

                    this.appendDummyInput()
                        .appendField("Ilość dób")
                        .appendField(new Blockly.FieldTextInput("1"), "days");
                    this.setOutput(true, "Number");
                    this.setColour(270);
                    this.setTooltip("");
                    this.setHelpUrl("http://www.example.com/");

                }
            };

            Blockly.CSharp["dayincrease"] = function (block) {
                let dropdownFunctionValue = block.getFieldValue("functions");
                let dropdownFunctionParamValue = block.getFieldValue("functionsParameter");
                let textInputDaysValue = block.getFieldValue("days");

                let functionParamArray = [dropdownFunctionValue, dropdownFunctionParamValue, textInputDaysValue];

                let functionParamString = functionParamArray.join(", ");

                let order = Blockly.CSharp.ORDER_ATOMIC;

                let code = "GetDaysIncrease(" + functionParamString + ").Value";
                return [code, Blockly.CSharp.ORDER_NONE];
            };


            Blockly.JavaScript["dayincrease"] = function (block) {
                let dropdownFunctionValue = block.getFieldValue("functions");
                let dropdownFunctionParamValue = block.getFieldValue("functionsParameter");
                let textInputDaysValue = block.getFieldValue("days");

                let functionParamArray = [dropdownFunctionValue, dropdownFunctionParamValue, textInputDaysValue];

                let functionParamString = functionParamArray.join(", ");

                let order = Blockly.JavaScript.ORDER_ATOMIC;

                let code = "GetDaysIncrease(" + functionParamString + ").Value";
                return [code, Blockly.JavaScript.ORDER_NONE];
            };
        }
    }
}