/// <reference path="../../common.d.ts" />

namespace Exdata {
    export class SimpleDataService {
        static $inject = ["$q"];
        static Name = "SimpleDataService";

        private qService: ng.IQService;

        constructor($q: ng.IQService) {
            this.qService = $q;
        }

        public getFunctionsPromise = (idString: string): ng.IPromise<string[][]> => {
            let id = parseInt(idString);
            let defer = this.qService.defer();

            let functions = [["Ciepłomierz C.O.", "2"],
                ["Ciepłomierz CT 1", "4"],
                ["Ciepłomierz CT 2", "5"],
                ["Wodomierz Główny", "6"]];

            defer.resolve(functions);

            return defer.promise;
        }

        public getFunctionsParameterPromise = (idString: string): ng.IPromise<string[][]> => {
            let id = parseInt(idString);
            let defer = this.qService.defer();
            let functions: string[][];
            switch (id) {
                case 6:
                    functions = [
                        ["Objętość", "3"],
                        ["Sygnał", "4"]

                    ];

                    break;
                case 2:
                case 4:
                case 5:
                default:
                    functions = [
                        ["Energia", "1"],
                        ["Sygnał", "2"]
                    ];
                    break;
            }

            defer.resolve(functions);

            return defer.promise;
        }



        public getFunctions = (id: number): string[][] => {

            let functions = [["Ciepłomierz C.O.", "2"],
                ["Ciepłomierz CT 1", "4"],
                ["Ciepłomierz CT 2", "5"],
                ["Ciepłomierz CT 3", "6"]];



            return functions;
        }
    }
}