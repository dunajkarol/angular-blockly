var concat = {
  options: {
    stripBanners: true
  },
  module: {
    src: [

      "dist/build/ts/blockly/directives/angularblocklydirective.js",
      "dist/build/ts/blockly/controllers/angularblocklycontroller.js",
      "dist/build/ts/blockly/providers/angularblocklyoptionsprovider.js",
      "dist/build/ts/blockly/services/angularblockyservice.js",
      "dist/build/ts/blockly/blocklymodule.js"
    ],
    dest: 'dist/release/js/angular-blockly.js',
    nonull: true
  },
  moduleDeclaration: {
    src: [
      "src/ts/blockly/typings/**/*.ts",
      "!src/ts/blockly/typings/googTypings/*.*"
    ],
    dest: 'dist/release/typings/angular-blockly.d.ts',
    nonull: true
  },
  blocklyLib: {
    src: [
      "dist/build/lib/got-blockly/storage.js",
      "dist/build/lib/got-blockly/blockly_compressed.js",
      "dist/build/lib/got-blockly/blocks_compressed.js",
      "dist/build/lib/got-blockly/javascript_compressed.js",
      "dist/build/lib/got-blockly/generators/csharp.js",
      "dist/build/lib/got-blockly/generators/csharp/colour.js",
      "dist/build/lib/got-blockly/generators/csharp/lists.js",
      "dist/build/lib/got-blockly/generators/csharp/logic.js",
      "dist/build/lib/got-blockly/generators/csharp/loops.js",
      "dist/build/lib/got-blockly/generators/csharp/math.js",
      "dist/build/lib/got-blockly/generators/csharp/procedures.js",
      "dist/build/lib/got-blockly/generators/csharp/text.js",
      "dist/build/lib/got-blockly/generators/csharp/variables.js",
    ],
    dest: 'dist/release/js/blockly-lib.js',
    nonull: true
  },
  blocklyLibDeclaration: {
    src: [
      "src/ts/blockly/googTypings/**/*.d.ts",
      "src/ts/blockly/gapiTypings/**/*.d.ts",
      "src/ts/blockly/blockly-google.d.ts"
    ],
    dest: 'dist/release/typings/blockly-lib.d.ts',
    nonull: true
  },
}
module.exports = function (grunt) {
  grunt.config.set('concat', concat);
}
