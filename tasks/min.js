var cssmin = {
  options: {
    preserveComments: false,
    sourceMap: false,
    report: 'gzip'
  },
  dist: {
    src: ['dist/release/css/blockly-src.css'],
    dest: 'dist/release/css/blockly.css'
  }
};
var uglify = {
  options: {
    preserveComments: false,
    sourceMap: false,
    report: 'gzip'
  },
  dist: {
    src: 'dist/release/blockly-src.js',
    dest: 'dist/release/blockly.js'
  }
};
module.exports = function (grunt) {
  grunt.config.set('cssmin', cssmin);
  grunt.config.set('uglify', uglify);
}
