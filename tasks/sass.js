var sass = {
  build: {
    options: {
      trace: true,
      debugInfo: true,
      sourcemap: false,
      style: 'expanded',
      noCache: true
    },
    files: {
      'dist/build/sass/styles.css': 'src/sass/styles.scss'
    }
  },
  release: {
    options: {
      sourcemap: "none",
      noCache: true
    },
    files: {
      'dist/release/css/angular-blockly.css': 'src/sass/blockly.scss'
    }
  }
}
module.exports = function (grunt) {
  grunt.config.set('sass', sass);
}