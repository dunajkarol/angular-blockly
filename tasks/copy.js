var copy = {
  external: {
    files: [
      { expand: true, cwd: 'external/angular/', src: ['angular.min.js', 'angular.js'], dest: 'src/lib/angular' },
      { expand: true, cwd: 'external/angular-aria/', src: ['*.js', '*.css'], dest: 'src/lib/angular-aria' },
      { expand: true, cwd: 'external/angular-animate/', src: ['**/*.js'], dest: 'src/lib/angular-animate' },
      { expand: true, cwd: 'external/angular-material/', src: ['*.js', '*.css'], dest: 'src/lib/angular-material' },
      { expand: true, cwd: 'external/angular-route/', src: ['angular-route.js', 'angular-route.min.js'], dest: 'src/lib/angular' },
      { expand: true, cwd: 'external/angular-mocks/', src: ['angular-mocks.js', 'ngAnimateMock.js', 'nkMock.js', 'ngMockE2E.js'], dest: 'src/lib/angular-tests' },
      { expand: true, cwd: 'external/got-blockly/', src: ['generators/**/*.js', 'media/**/*.*', 'msg/**/*.*', '*.js'], dest: 'src/lib/got-blockly' },
      { expand: true, cwd: 'external/got-blockly/demos/code/', src: ['style.css'], dest: 'src/lib/got-blockly/css' },
    ]
  },
  // release: {
  //   files: [
  //     { expand: true, cwd: 'src/typings/angular-blockly.d.ts', src: [
  //         '',
  //       ], dest: 'dist/release/typings'
  //     },
  //   ]
  // },
  initBuild: {
    files: [
      { expand: true, cwd: 'src', src: ['**', '!**/*.ts', '!**/*.scss'], dest: 'dist/build' },
      { expand: true, cwd: 'src/ts/blockly', src: ['files/**/*.*'], dest: 'dist/build/lib/blockly' },
    ]
  },
  htmlBuild: {
    files: [
      { expand: true, cwd: 'src', src: ['**/*.html'], dest: 'dist/build' },
    ]
  },
  release: {
    files: [
      { expand: true, cwd: 'src/ts/blockly', src: ['files/**/*.*', 'msg/**/*.*'], dest: 'dist/release/assets' },
      { expand: true, cwd: 'dist/build/lib/got-blockly', src: ['msg/**/*.*', 'media/**/*.*'], dest: 'dist/release/assets' },
      { expand: true, cwd: 'dist/build/lib/got-blockly/css', src: ['style.css'], dest: 'dist/release/css' },
    ]

  }
};

module.exports = function (grunt) {
  grunt.config.set('copy', copy);
}
